#include "Widgets.h"

// ******** 8 Colours RGB LED ********
#define DEFAULT_DELAY 250

BasicRGBLED::BasicRGBLED(byte redPin, byte greenPin, byte bluePin) {
    _r_p = redPin;
    _g_p = greenPin;
    _b_p = bluePin;

    pinMode(_r_p, OUTPUT);
    pinMode(_g_p, OUTPUT);
    pinMode(_b_p, OUTPUT);

    off();
}

void BasicRGBLED::colour(Colour c) {
    switch (c) {
        case RED:
            _set_colour(HIGH, LOW, LOW);
            break;
        case GREEN:
            _set_colour(LOW, HIGH, LOW);
            break;
        case BLUE:
            _set_colour(LOW, LOW, HIGH);
            break;
        case YELLOW:
            _set_colour(HIGH, HIGH, LOW);
            break;
        case VIOLET:
            _set_colour(HIGH, LOW, HIGH);
            break;
        case CYAN:
            _set_colour(LOW, HIGH, HIGH);
            break;
        case WHITE:
            _set_colour(HIGH, HIGH, HIGH);
            break;
        default:
            off();
    }
}

void BasicRGBLED::blink(Colour c, int count) {
    blink(c, count, DEFAULT_DELAY, DEFAULT_DELAY);
}

void BasicRGBLED::blink(Colour c, int count, int interval) {
    blink(c, count, interval, interval);
}

void BasicRGBLED::blink(Colour c, int count, int onInterval, int offInterval) {
    for (int i = 0; i < count; i++) {
        colour(c);
        delay(onInterval);
        off();
        delay(offInterval);
    }
}

void BasicRGBLED::off() {
    digitalWrite(_r_p, LOW);
    digitalWrite(_g_p, LOW);
    digitalWrite(_b_p, LOW);
}

inline void BasicRGBLED::_set_colour(byte r, byte g, byte b) {
    digitalWrite(_r_p, r);
    digitalWrite(_g_p, g);
    digitalWrite(_b_p, b);
}

// ******** BUTTON ********
Button::Button(byte pin) {
    _pin = pin;
    pinMode(_pin, INPUT_PULLUP);
}

byte Button::state() {
    return digitalRead(_pin);
}

bool Button::is_up() {
    return (digitalRead(_pin) == HIGH);
}

bool Button::is_down() {
    return (digitalRead(_pin) == LOW);
}

int Button::duration(unsigned int maxDuration) {
    if (is_up()) return 0;
    elapsedMillis duration;
    while (duration < maxDuration) {
        if (is_up()) {
            break;
        }
    }
    int t = duration;
    return t;
}