#include "DirectionalJoystick.h"
#include <Arduino.h>

#define X 0
#define Y 1

DirectionalJoystick::DirectionalJoystick(int vcc_p, int jx_p, int jy_p, int mid, int buf) {
    vcc_pin = vcc_p;
    x_pin = jx_p;
    y_pin = jy_p;
    midpoint = mid;
    bufferSize = buf;

    pinMode(vcc_pin, OUTPUT);
    digitalWrite(vcc_pin, LOW);
}

void DirectionalJoystick::update() {
    digitalWrite(vcc_pin, HIGH);
    values[X] = analogRead(x_pin);
    values[Y] = analogRead(y_pin);
    digitalWrite(vcc_pin, LOW);
}

bool DirectionalJoystick::is_neutral() {
    int dx = abs(values[X] - midpoint);
    int dy = abs(values[Y] - midpoint);

    return (dx < bufferSize && dy < bufferSize) ? true : false;
}

int DirectionalJoystick::which_zone(int d) {
    if (abs(d - midpoint) < bufferSize) return 0; 
    return (d > midpoint) ? 2 : 1;
}

Direction DirectionalJoystick::get_direction() {
    int d = (which_zone(values[X]) << 4) | which_zone(values[Y]);

    switch (d) {
        case 0x02:
            return UP;
        case 0x01:
            return DOWN;
        case 0x20:
            return RIGHT;
        case 0x10:
            return LEFT;
        case 0x12:
            return UP_LEFT;
        case 0x22:
            return UP_RIGHT;
        case 0x11:
            return DOWN_LEFT;
        case 0x21:
            return DOWN_RIGHT;
        default:
            return NEUTRAL;
    }
}