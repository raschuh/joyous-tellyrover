#include "TWIROM.h"
#include <Wire.h>

void twirom_set_iadr(uint8_t dadr, uint8_t iadr) {
    Wire.beginTransmission(dadr);
    Wire.write(iadr);
    Wire.endTransmission();
    delay(50);
}

void twirom_write_byte(uint8_t dadr, uint8_t iadr, uint8_t data) {
    Wire.beginTransmission(dadr);
    Wire.write(iadr);
    Wire.write(data);
    Wire.endTransmission();
    delay(50);
}

uint8_t twirom_current_read(uint8_t dadr) {
    Wire.requestFrom(dadr, 1);
    return (Wire.available()) ? Wire.read() : 0xFF;
}

uint8_t twirom_read_byte(uint8_t dadr, uint8_t iadr) {
    twirom_set_iadr(dadr, iadr);
    return twirom_current_read(dadr);
}