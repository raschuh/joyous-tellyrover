#include <Arduino.h>
#include <Snooze.h>
#include <Wire.h>
#include <string.h>
#include "TimingMacros.h"
#include "Widgets.h"
#include "DirectionalJoystick.h"
#include "IRController.h"
#include "TWIROM.h"

#define RED_LED_PIN        8
#define GREEN_LED_PIN      7
#define BLUE_LED_PIN       6
#define EN_SW_PIN          4
#define JOY_VCC_PIN        21
#define JOY_VX_PIN         A8
#define JOY_VY_PIN         A9
#define JOY_MID_VAL        512
#define JOY_BUFF_VAL       402

#define EEPROM_ADDR        0b1010000

BasicRGBLED g_led(RED_LED_PIN, GREEN_LED_PIN, BLUE_LED_PIN);
Button g_button(EN_SW_PIN);

SnoozeTimer g_sn_timer;
SnoozeDigital g_sn_digi;
SnoozeBlock g_sn_config(g_sn_digi, g_sn_timer);

IRController g_ir_dev;

DirectionalJoystick g_joy(JOY_VCC_PIN, JOY_VX_PIN, JOY_VY_PIN, JOY_MID_VAL, JOY_BUFF_VAL);

void program_mode();
void joystick_mode();
bool capture_samples(IRCode samples[], int sample_size);
bool validate_samples(IRCode samples[], int sample_size);
void get_command_from_eeprom(Direction dir, IRCode &result);
void write_to_eeprom(Direction dir, IRCode code);

void setup() {
    Wire.begin();
    g_sn_digi.pinMode(EN_SW_PIN, INPUT_PULLUP, FALLING);
    g_sn_timer.setTimer(TVJOY_SLEEP_TIME);
    (map(analogRead(A0), 0, 1023, 0, 3300) > 1900) ? g_led.blink(GREEN, 1) : g_led.blink(RED, 3);
}

void loop() {
    (Snooze.sleep(g_sn_config) == TIMER_WAKE) ? joystick_mode() : program_mode();
}

void joystick_mode() {
    g_joy.update();
    if (g_joy.is_neutral()) return;
    Direction dir = g_joy.get_direction();
    IRCode command;
    get_command_from_eeprom(dir, command);
    g_ir_dev.transmit(command);
}

void program_mode() {
    if (g_button.duration(MAX_PRESS_DURATION) < THREE_SECONDS) return;

    g_led.blink(YELLOW, 1, ONE_SECOND);
    g_ir_dev.rx_on();
    g_led.colour(GREEN);

    elapsedMillis timeout;

    while (timeout < TEN_SECONDS) {
        g_joy.update();
        if (g_joy.is_neutral()) continue;

        Direction dir = g_joy.get_direction();
        IRCode samples[3];

        g_led.blink(GREEN, 3);
        g_led.colour(VIOLET);

        if (!capture_samples(samples, 3)) {
            g_led.blink(RED, 3, ONE_EIGHTH_SEC);
            g_led.colour(GREEN);
            timeout = 0;
            continue;
        }

        if (!validate_samples(samples, 3)) {
            g_led.blink(RED, 3, ONE_EIGHTH_SEC);
            g_led.colour(GREEN);
            timeout = 0;
            continue;
        }

        g_led.blink(GREEN, 3, ONE_EIGHTH_SEC);
        WAIT_500_MS
        g_led.blink(CYAN, 3, QUARTER_SECOND);
        g_led.colour(CYAN);
        write_to_eeprom(dir, samples[0]);
        g_led.off();
        WAIT_1_S
        timeout = 0;
        g_led.colour(GREEN);
    }

    g_led.blink(BLUE, 3, ONE_EIGHTH_SEC);
    g_ir_dev.rx_off();
}

bool capture_samples(IRCode samples[], int sample_size) {
    for (int i = 0; i < sample_size; i++) {
        if (!g_ir_dev.receive(samples[i])) return false;
        g_led.blink(WHITE, 1);
    }
    return true;
}

bool validate_samples(IRCode samples[], int sample_size) {
    IRCode head = samples[0];
    for (int i = 1; i < sample_size; i++) {
        if (samples[i].value == 0xFFFFFFFF || samples[i].value == 0x0) continue;
        if (samples[i].value != head.value) return false;
    }
    return true;
}

void get_command_from_eeprom(Direction dir, IRCode &result) {
    int dadr = EEPROM_ADDR + dir;
    twirom_set_iadr(dadr, 0);
    result.size = twirom_current_read(dadr);
    if (result.size == 0) return;
    for (int i = 0; i < result.size; i++) {
        result.ticks[i] = (twirom_current_read(dadr) * USECPERTICK);
    }
}

void write_to_eeprom(Direction dir, IRCode code) {
    int dadr = EEPROM_ADDR + dir;
    twirom_write_byte(dadr, 0, code.size);
    if (code.size == 0) return;
    for (int i = 0; i < code.size; i++) {
        twirom_write_byte(dadr, i + 1, code.ticks[i] / USECPERTICK);
    }
}