#include "IRController.h"
#include <Arduino.h>

#define RX_PIN      3
#define RX_POW_PIN  5
#define GAP_SIZE    250     // ms
#define WAIT_TIME   10000   // 10 secs
#define TX_FREQ     38      // 38 kHz as the transmit frequency

IRController::IRController() : rx(RX_PIN) {
    pinMode(RX_POW_PIN, OUTPUT);
    rx.enableIRIn();
    rx_off();
}

void IRController::transmit(IRCode irCode) {
    tx.sendRaw(irCode.ticks, irCode.size, TX_FREQ);
    delay(GAP_SIZE);
}

bool IRController::receive(IRCode &code) {
    decode_results results;
    elapsedMillis wait;
    while (wait < WAIT_TIME) {
        if (rx.decode(&results)) {
            process(results, code);
            rx.resume();
            return true;
        }
    }
    return false;
}

void IRController::rx_off() {
    digitalWrite(RX_POW_PIN, LOW);
}

void IRController::rx_on() {
    digitalWrite(RX_POW_PIN, HIGH);
}

void IRController::process(decode_results &results, IRCode &code) {
    code.size = results.rawlen - 1;
    code.value = results.value;

    for (int i = 0; i < results.rawlen; i++) {
        long got = results.rawbuf[i + 1] * USECPERTICK;
        got = (i % 2 == 0) ? got - MARK_EXCESS : got + MARK_EXCESS;
        code.ticks[i] = got;
    }
}  