/****************************************************************************
 * 
 * Helper functions for 24LCxx series I2C EEPROMs
 * 
 * Robert Schuh
 * 27 March 2019
 * 
 ***************************************************************************/

#ifndef TVJOY_TWIROM_H
#define TVJOY_TWIROM_H

#include <Arduino.h>

void twirom_set_iadr(uint8_t dadr, uint8_t iadr);
void twirom_write_byte(uint8_t dadr, uint8_t iadr, uint8_t data);
uint8_t twirom_current_read(uint8_t dadr);
uint8_t twirom_read_byte(uint8_t dadr, uint8_t iadr);

#endif