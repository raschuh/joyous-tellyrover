#ifndef TVJOY_WIDGETS_H
#define TVJOY_WIDGETS_H

#include <Arduino.h>

enum Colour {
    RED,
    GREEN,
    BLUE,
    YELLOW,
    VIOLET,
    CYAN,
    WHITE,
    BLACK
};

class BasicRGBLED {
public:
    BasicRGBLED(byte redPin, byte greenPin, byte bluePin);
    void colour(Colour c);
    void blink(Colour c, int count);
    void blink(Colour c, int count, int interval);
    void blink(Colour c, int count, int onInterval, int offInterval);
    void off();

private:
    byte _r_p;
    byte _g_p;
    byte _b_p;

    void _set_colour(byte r, byte g, byte b);
};

class Button {
public:
    Button(byte pin);
    byte state();
    bool is_up();
    bool is_down();
    int duration(unsigned int maxDuration);

private:
    byte _pin;
};

#endif