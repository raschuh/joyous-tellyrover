#ifndef TVJOY_IRCONTROLLER_H
#define TVJOY_IRCONTROLLER_H

#include <IRremote.h>

typedef struct {
    unsigned int value;
    unsigned int ticks[200];
    int size;
} IRCode;

class IRController {
public:
    IRController();
    void transmit(IRCode irCode);
    bool receive(IRCode &code);
    void rx_off();
    void rx_on();

private:
    IRsend tx; // Pin 16 on Teensy LC
    IRrecv rx; // Pin 3 has been selected for Teensy LC

    void process(decode_results &results, IRCode &code); 
};

#endif