#ifndef TVJOY_DIRECTIONALJOYSTICK_H
#define TVJOY_DIRECTIONALJOYSTICK_H

enum Direction {
    NEUTRAL = -1,
    UP,
    UP_RIGHT,
    RIGHT,
    DOWN_RIGHT,
    DOWN,
    DOWN_LEFT,
    LEFT,
    UP_LEFT
};

class DirectionalJoystick {
public:
    DirectionalJoystick(int vcc_p, int jx_p, int jy_p, int mid, int buf);
    void update();
    bool is_neutral();
    Direction get_direction();

private:
    int vcc_pin;
    int x_pin;
    int y_pin;
    int midpoint;
    int bufferSize;
    int values[2];

    int which_zone(int d);
};

#endif