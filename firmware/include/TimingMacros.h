#ifndef TVJOY_TIMINGMACROS_H
#define TVJOY_TIMINGMACROS_H

#define MAX_PRESS_DURATION 3025
#define TEN_SECONDS        10000
#define FIVE_SECONDS       5000
#define THREE_SECONDS      3000
#define ONE_SECOND         1000
#define HALF_SECOND        500
#define QUARTER_SECOND     250
#define ONE_EIGHTH_SEC     125

#define TVJOY_SLEEP_TIME   1000

#define WAIT_3_S        delay(3000);
#define WAIT_1_S        delay(1000);
#define WAIT_500_MS     delay(500);

#endif