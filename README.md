# Joyous Tellyrover
-------------------
Pardon the title... The project title was formerly "Finger Channel Surfer" and so renaming was needed. Still, "Joyous Tellyrover" is better than "Finger Channel Surfer." :)

## Introduction

#### "Universal TV Remote Joystick"

TOM Calgary, a non-profit organization with a goal of developing low-cost and open source accessibility technology, hosted a 72-hour makeathon in Calgary on August 2017.
A team of three - myself as project manager and embedded developer, a mechanical engineer, and a nurse came together to take on one of the projects offered by the makeathon. 

Our project: construct a custom TV remote for a quadriplegic man, so he can control his TV independently. 
He cannot use a standard TV remote as it was difficult for him to reach and press any button, so his father had to come in to operate the remote for him.

Our team came up with an idea of a joystick based TV remote as the interface is familiar to him and easy for him to use. The direction of a joystick movement will determine which IR command to send - 
i.e., an upward movement will send a 'Channel +' IR command to the cable box. The user will use the cable box's remote to program the joystick remote by pressing the remote's button for each joystick direction. 
The joystick remote supports eight commands/directions.

The joystick base designed by the mech engineer and 3D printed by Javelin Technologies, a Calgary based 3D printing service. I designed and developed the electronics and firmware. 
We were able to deliver a working prototype by the end of the makeathon. 

As of April 2019, the final product is still a work in progress with the electronic circuitry and firmware effectively implemented. The features are listed below.

### Update 

The project is on hiatus indefinitely due to the dissolution of the TOM organization and the lack of interest from all parties involved. 

## Features

* A large joystick with low stiffness so the user can effortlessly move the stick around 
* One IR command each of the eight possible joystick direction (horizontal, vertical, and diagonal)
* Efficient power management - two AA batteries should last up to a year and poll the joystick input at a time interval
* Power button to conserve the batteries while the remote is not in use for a long period of time
* Programming the device using a remote controller
* Large and sturdy base so the user have it resting beside him on his bed

## Hardware

The hardware platform features:

* Teensy LC microcontroller board
* Joystick
* IR LED emitter
* TSOP38238 IR Receiver
* RGB LED
* Electronic latching pushbutton switch 

The joystick itself is a large RC type joystick. The base is made of thick and heavy plastic which was printed by an industrial grade 3D printer. The RGB LED is used to communicate the device states to the user.

## Firmware

The firmware is written in C using Teensyduino, Arduino Wire, [Snooze](https://github.com/duff2013/Snooze), and [IRremote](https://github.com/z3t0/Arduino-IRremote).
